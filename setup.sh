#!/bin/sh

module purge
module use /usr/local/software/spack/spack-modules/dpcpp-cuda-20220220/linux-centos8-x86_64_v3/
module load dpcpp
module load gcc/11.2.0

clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda test-sycl.cpp -o test-sycl
./test-sycl
